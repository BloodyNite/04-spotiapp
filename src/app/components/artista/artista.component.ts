import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, RouterLinkActive } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styles: []
})
export class ArtistaComponent {

artista: any = {};
topTracks: any[] = [];
loading: boolean;

  constructor(private router: ActivatedRoute, private spotify: SpotifyService) {
    this.loading = true;
    this.router.params.subscribe( params =>{
      // tslint:disable-next-line: no-string-literal// tslint:disable-next-line: no-string-literal
      this.getArtista(params['id']);
      // tslint:disable-next-line: no-string-literal
      this.getTopTracks(params['id']);
      // tslint:disable-next-line: no-string-literal
      console.log(params['id']);
    });
  }

getArtista(id: string){
  this.loading = true;
this.spotify.getArtista(id)
    .subscribe(artista =>{
      console.log(artista);
      this.artista = artista;
      this.loading = false;
    })
}
getTopTracks(id){
  this.spotify.getTopTrack(id)
      .subscribe(topTracks => {
        console.log(topTracks);
        this.topTracks = topTracks;
      })
}

}
