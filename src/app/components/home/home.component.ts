import { Component, OnInit } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: []
})
export class HomeComponent implements OnInit {

nuevasCanciones: any[] = [];
loading: boolean;
error: boolean;
mensajeError: string;

  constructor(private spotify: SpotifyService) {
    this.error = false;
    this.loading = true;

    this.spotify.getNewReleases()
        .subscribe((data:any) =>{
          this.nuevasCanciones = data;
          this.loading = false;
        },(errorServicio)=>{
          this.error = true;
          this.loading = false;
          this.mensajeError = errorServicio.error.error.message;
        });

  }

  ngOnInit() {
  }

}
