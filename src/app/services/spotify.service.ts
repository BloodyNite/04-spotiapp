import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) {
    console.log('SpotifyService');
  }
getQuery(query: string) {
  const url = `https://api.spotify.com/v1/${query}`;
  const headers = new HttpHeaders({
    // tslint:disable-next-line: max-line-length
    Authorization: 'Bearer BQAW3fBxhRJA2Qqh7GruaS8aEsWmgW2_y9aiZnl1byPckAKtR5Pq0flvxj6CFWBci7D7L8t1KsW1sGRdtjM'
  });

  return this.http.get(url, {headers});
}

  getNewReleases() {
    return this.getQuery('browse/new-releases?limit=20')
    // tslint:disable-next-line: no-string-literal
    .pipe(map(data => data['albums'].items));
  }

  getArtistas(termino: string) {
    return this.getQuery(`search?q=${termino}&type=artist&limit=20`)
    // tslint:disable-next-line: no-string-literal
    .pipe(map(data => data['data'].items
    ));

    // return this.http.get(`https://api.spotify.com/v1/search?q=${termino}&type=artist&limit=20`, {headers})

  }
  getArtista(id: string) {
    return this.getQuery(`artists/${id}`);
    // tslint:disable-next-line: no-string-literal
    // .pipe(map(data => data['artists'].items
    // ));
}
getTopTrack(id: string) {
  return this.getQuery(`artists/${id}/top-tracks?country=us`)
  // tslint:disable-next-line: no-string-literal
  .pipe(map(data => data['tracks']
  ));
}
}
